export interface Pessoa {
  nome: string;
  dataNascimento: string;
  cpf: string;
  estadoCivil:string;
}