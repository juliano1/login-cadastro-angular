import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModaldadoscadastraisComponent } from './modaldadoscadastrais.component';

describe('ModaldadoscadastraisComponent', () => {
  let component: ModaldadoscadastraisComponent;
  let fixture: ComponentFixture<ModaldadoscadastraisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModaldadoscadastraisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModaldadoscadastraisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
