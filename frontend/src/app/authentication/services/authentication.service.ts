import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../../shared/interfaces/client';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  usuarioLogado: boolean = false;

  private readonly url: string = `${environment.API}client`;

  constructor(private httpClient: HttpClient) {}

  getAllUsers(): Observable<Client[]> {
    return this.httpClient.get<Client[]>(this.url);
  }

  editUser(user: Client, id: number | undefined): Observable<Client> {
    return this.httpClient.put<Client>(`${this.url}/${id}`, user);
  }

  public isAuthenticated(): boolean {
    return this.usuarioLogado;
  }
}
