import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalRedefinirSenhaComponent } from './modal-redefinir-senha/modal-redefinir-senha.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SharedService } from '../../shared/shared.service';
import { AuthenticationService } from '../../authentication/services/authentication.service';
import { Client } from '../../shared/interfaces/client';

@Component({
  selector: 'app-redefinir-senha',
  templateUrl: './redefinir-senha.component.html',
  styleUrls: ['./redefinir-senha.component.css'],
})
export class RedefinirSenhaComponent implements OnInit {
  formRedefinirSenha!: FormGroup;
  codigoConf?: number;
  msgErro: string = '';

  usuario!: Client;

  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private authService: AuthenticationService
  ) {}

  ngOnInit(): void {
    this.formRedefinirSenha = this.formBuilder.group({
      cpf: [
        '',
        [
          Validators.required,
          Validators.pattern(this.sharedService.regexValidarCpf),
        ],
      ],
      codigoConfirmacao: [
        '',
        [Validators.required, Validators.minLength(6), Validators.maxLength(6)],
      ],
      novaSenha: [
        '',
        [
          Validators.required,
          Validators.pattern(this.sharedService.regexValidarSenha),
        ],
      ],
      novaSenhaConfirmacao: [
        '',
        [
          Validators.required,
          Validators.pattern(this.sharedService.regexValidarSenha),
        ],
      ],
    });
  }

  getUserByCpf(cpf: string): void {
    this.authService.getAllUsers().subscribe((users) => {
      this.usuario = users.filter((user) => user.cpf === cpf)[0];
      this.alterarSenhaUsuario(this.usuario)
        ? this.authService.editUser(this.usuario, this.usuario.id).subscribe()
        : alert(this.msgErro);
    });
  }

  cadastrarNovaSenha(): void {
    if (!this.formRedefinirSenha.valid) {
      return;
    }
    this.getUserByCpf(this.formRedefinirSenha.controls['cpf'].value);
  }

  alterarSenhaUsuario(user: Client): boolean {
    if (!user) {
      this.msgErro = 'Nenhum usuário com esse CPF foi encontrado';
      return false;
    }

    if (
      !(
        Number(this.formRedefinirSenha.controls['codigoConfirmacao'].value) ===
        this.codigoConf
      )
    ) {
      this.msgErro = 'Código incorreto';
      return false;
    }

    if (
      !(
        this.formRedefinirSenha.controls['novaSenha'].value ===
        this.formRedefinirSenha.controls['novaSenhaConfirmacao'].value
      )
    ) {
      this.msgErro = 'As senhas informadas são diferentes';
      return false;
    }

    user.senha = this.formRedefinirSenha.controls['novaSenha'].value;
    alert('Senha alterada com sucesso');
    this.formRedefinirSenha.reset();
    return true;
  }

  exibirCodigo() {
    this.codigoConf = this.gerarCodigoConfirmacao();
    alert(`Seu código de confirmação é: ${this.codigoConf}`);
  }

  private gerarCodigoConfirmacao() {
    let min = Math.ceil(100000);
    let max = Math.floor(999999);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  open() {
    const modalRef = this.modalService.open(ModalRedefinirSenhaComponent);
    modalRef.componentInstance.name = 'World';
  }
}
