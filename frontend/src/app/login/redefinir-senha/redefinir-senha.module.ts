import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RedefinirSenhaComponent } from './redefinir-senha.component';
import { SharedModule } from '../../shared/shared.module';
import { ModalRedefinirSenhaComponent } from './modal-redefinir-senha/modal-redefinir-senha.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [RedefinirSenhaComponent, ModalRedefinirSenhaComponent],
  imports: [CommonModule, SharedModule, FormsModule, ReactiveFormsModule],
})
export class RedefinirSenhaModule {}
