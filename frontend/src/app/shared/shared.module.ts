import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageComponent } from './components/page/page.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [PageComponent, HeaderComponent, FooterComponent],
  imports: [CommonModule, RouterModule],
  exports: [PageComponent],
})
export class SharedModule {}
