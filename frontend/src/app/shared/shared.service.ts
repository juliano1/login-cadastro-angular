import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SharedService {
  readonly regexValidarCpf: RegExp =
    /^([0-9]{3}).?([0-9]{3}).?([0-9]{3})-?([0-9]{2})$/;
  readonly regexValidarSenha: RegExp =
    /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;

  constructor() {}
}
