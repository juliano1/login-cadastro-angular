import { Injectable } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FeedbackModalComponent } from './feedback-modal.component';

export enum PROPERTIES {
  danger = 'danger',
  success = 'success'
}

@Injectable({
  providedIn: 'root'
})
export class FeedbackModalService {

bsModalRef?: BsModalRef

constructor(private bsModalService: BsModalService) { }

giveFeedBackModal(message: string, property: string){
  this.bsModalRef = this.bsModalService.show(FeedbackModalComponent)
  this.bsModalRef.content.property = property
  this.bsModalRef.content.message = message
}

getError(message: string){
  this.giveFeedBackModal(message, PROPERTIES.danger)
}

getSuccess(message: string){
  this.giveFeedBackModal(message, PROPERTIES.success)
}

}
