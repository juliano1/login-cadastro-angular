/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FeedbackModalService } from './feedback-modal.service';

describe('Service: FeedbackModal', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FeedbackModalService]
    });
  });

  it('should ...', inject([FeedbackModalService], (service: FeedbackModalService) => {
    expect(service).toBeTruthy();
  }));
});
