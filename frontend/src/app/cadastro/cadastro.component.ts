import { ConnectionStatusEnum, NetworkConnection } from './NetworkConnection';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormValidators } from './../shared/validators/form-validators';
import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { map, take } from 'rxjs/operators';

import { CadastroClienteService } from './services/cadastro-cliente.service';
import { FeedbackModalService } from './feedback-modal/feedback-modal.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css'],
})
export class CadastroComponent implements OnInit {
  form?: FormGroup;
  bsModalRef?: BsModalRef;

  constructor(
    private cadastro: CadastroClienteService,
    private formBuilder: FormBuilder,
    private feedbackModalService: FeedbackModalService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      nome: [
        ,
        /*setar um valor inicial*/ [
          Validators.required,
        ] /*validações síncronas*/,
        [] /*validações assíncronas*/,
      ],
      cpf: [
        ,
        [
          Validators.required,
          Validators.minLength(11),
          FormValidators.isValidCPF,
        ],
        [this.verifyCpf.bind(this)],
      ],
      email: [, [Validators.required, Validators.email]],
      contatoCelular: [
        ,
        [
          Validators.required,
          Validators.minLength(11),
          FormValidators.isPhoneValid,
        ],
      ],
      dataNascimento: [, [Validators.required /*FormValidators.isDateValid*/]],
      renda: [, [Validators.required]],
      relacionamento: [, [Validators.required]],
      senha: [, [Validators.required, FormValidators.isPasswordValid]],
      comfirmeSenha: [
        ,
        [
          Validators.required,
          FormValidators.isPasswordValid,
          FormValidators.equalsTo('senha'),
        ],
      ],
      cep: [, [Validators.required, FormValidators.isCepValid]],
    });
  }

  onSubmit() {
    console.log(this.form);
    this.cadastro
      .create(this.form?.value)
      .pipe(take(1))
      .subscribe(
        (success) =>
          this.feedbackModalService.getSuccess(
            'Formulário enviado com sucesso!'
          ),
        (error) =>
          this.feedbackModalService.getError(
            'Erro ao conectar com o servidor. Tente novamente mais tarde.'
          )
      );
  }

  alreadyExistCpf(_cpf: string) {
    return this.cadastro.get().pipe(
      map((cadastro) => cadastro.map((cadastro) => cadastro.cpf)),
      //tap(cadastro => console.log(cadastro)),
      map((cpf) => cpf.filter((cpf) => cpf == _cpf)),
      map((cpf) => cpf.length > 0)
      //tap(console.log)
    );
  }

  verifyCpf(control: FormControl) {
    return this.alreadyExistCpf(control.value).pipe(
      map((cpf) => (cpf ? { alredyExistsCpf: true } : null))
    );
  }
}
