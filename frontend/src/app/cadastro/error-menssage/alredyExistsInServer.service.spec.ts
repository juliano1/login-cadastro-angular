/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AlredyExistsInServerService } from './alredyExistsInServer.service';

describe('Service: AlredyExistsInServer', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AlredyExistsInServerService]
    });
  });

  it('should ...', inject([AlredyExistsInServerService], (service: AlredyExistsInServerService) => {
    expect(service).toBeTruthy();
  }));
});
