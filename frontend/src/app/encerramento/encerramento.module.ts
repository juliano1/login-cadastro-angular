import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmailComponent } from './email/email.component';
import { EncerramentoComponent } from './encerramento.component';
import { TelefoneComponent } from './telefone/telefone.component';
import { CodigoComponent } from './codigo/codigo.component';
import { FinalizacaoComponent } from './finalizacao/finalizacao.component';
import { JustificativaComponent } from './justificativa/justificativa.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

const ROUTES: Routes = [
  { path: '', component: EncerramentoComponent },
  { path: 'email', component: EmailComponent },
  { path: 'telefone', component: TelefoneComponent },
  { path: 'codigo', component: CodigoComponent },
  { path: 'justificativa', component: JustificativaComponent },
  { path: 'finalizacao', component: FinalizacaoComponent },
];

@NgModule({
  declarations: [
    EncerramentoComponent,
    EmailComponent,
    TelefoneComponent,
    CodigoComponent,
    FinalizacaoComponent,
    JustificativaComponent,
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(ROUTES),
    ReactiveFormsModule,
    SharedModule,
  ],
})
export class EncerramentoModule {}
