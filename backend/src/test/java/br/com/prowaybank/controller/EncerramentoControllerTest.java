package br.com.prowaybank.controller;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import br.com.prowaybank.conceitospoo.ContaBancaria;
import br.com.prowaybank.conceitospoo.PessoaFisica;

public class EncerramentoControllerTest {

	@Test
	public void EncerramentoControllerPFRetornaFalse() {
		EncerramentoController controller = new EncerramentoController();
		
		PessoaFisica usuario = new PessoaFisica();
		ContaBancaria conta = new ContaBancaria();
		BigDecimal saldo = new BigDecimal(10.000);
		
		conta.setValorEmConta(saldo);
		usuario.setContaBancaria(conta);
		usuario.setCpf("02775321321");
		
		String retorno = controller.encerrarContaCliente(usuario, usuario.getCpf());
		Assert.assertEquals(retorno, "Usu�rio possui saldo em conta corrente. Imposs�vel encerrar a conta.");
	}

	@Test
	public void EncerramentoControllerPFDesativadaRetornaFalse() {
		EncerramentoController controller = new EncerramentoController();
		
		PessoaFisica usuario = new PessoaFisica();
		ContaBancaria conta = new ContaBancaria();
		BigDecimal saldo = new BigDecimal(15.000);
		
		conta.setAtiva(false);
		conta.setValorEmConta(saldo);
		usuario.setContaBancaria(conta);
		usuario.setCpf("02775321321");
		
		String retorno = controller.encerrarContaCliente(usuario, usuario.getCpf());
		Assert.assertEquals(retorno, "Usu�rio n�o possui conta corrente ativa. Imposs�vel encerrar a conta.");
	}

	@Test
	public void EncerramentoControllerPFRetornaTrue() {
		EncerramentoController controller = new EncerramentoController();
		
		PessoaFisica usuario = new PessoaFisica();
		ContaBancaria conta = new ContaBancaria();
		BigDecimal saldo = new BigDecimal(0.0);

		conta.setValorEmConta(saldo);
		usuario.setContaBancaria(conta);
		usuario.setCpf("02775321321");
		
		String retorno = controller.encerrarContaCliente(usuario, usuario.getCpf());
		Assert.assertEquals(retorno, "Conta encerrada com sucesso");
	}
}