package br.com.prowaybank.conceitospoo;

import org.junit.Test;

import static org.junit.Assert.*;

public class PessoaFisicaTest {

    @Test
    public void deveGravarCpfSeEstiverNoPadraoCorreto() {
        PessoaFisica pf = new PessoaFisica();
        pf.setCpf("12345678900");
        String cpfCadastrado = pf.getCpf();
        assertEquals("12345678900", cpfCadastrado);
    }

    @Test
    public void naoDeveGravarCpfSeEstiverForaDoPadraoCorreto() {
        PessoaFisica pf = new PessoaFisica();
        pf.setCpf("123456789");
        String cpfCadastrado = pf.getCpf();
        assertNull(cpfCadastrado);
    }
}