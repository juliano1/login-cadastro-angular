package br.com.prowaybank.conceitospoo;

import org.junit.Test;

import static org.junit.Assert.*;

public class EnderecoTest {

    @Test
    public void deveGravarCepSomenteSeForValido() {
        Endereco endereco = new Endereco();
        endereco.setCep("98765432");
        String cepCadastrado = endereco.getCep();
        assertEquals("98765432", cepCadastrado);
    }

    @Test
    public void naoDeveGravarCepSeForInvalido() {
        Endereco endereco = new Endereco();
        endereco.setCep("98765");
        String cepCadastrado = endereco.getCep();
        assertNull(cepCadastrado);
    }
}