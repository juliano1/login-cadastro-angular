package br.com.prowaybank.conceitospoo;

import org.junit.Test;

import static org.junit.Assert.*;

public class UsuarioTest {

    @Test
    public void deveCadastrarTelefoneSeEstiverNoPadraoCorreto() { 
        Usuario usuario = new Usuario();
        usuario.setTelefone("11987654321");
        String telefoneCadastrado = usuario.getTelefone();
        assertEquals("11987654321", telefoneCadastrado);
    }

    @Test
    public void naoDeveCadastrarTelefoneSeEstiverForaDoPadraoCorreto() {
        Usuario usuario = new Usuario();  
        usuario.setTelefone("119876543");
        String telefoneCadastrado = usuario.getTelefone();
        assertNull(telefoneCadastrado);
    }  

    @Test
    public void deveCadastrarSenhaSeEstiverNoPadraoEsperado() {
        Usuario usuario = new Usuario();
        usuario.setSenha("Abc@@123");
        String senhaCadastrada = usuario.getSenha();
        assertEquals("Abc@@123", senhaCadastrada);
    }

    @Test
    public void naoDeveCadastrarSenhaSeEstiverForaDoPadraoEsperado() {
        Usuario usuario = new Usuario();
        usuario.setSenha("Abc@@");
        String senhaCadastrada = usuario.getSenha();
        assertNull(senhaCadastrada);
    }
    
    
}