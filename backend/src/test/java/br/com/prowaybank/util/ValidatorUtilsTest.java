package br.com.prowaybank.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;

import org.junit.Test;

public class ValidatorUtilsTest {

    @Test
    public void codigoConfirmacaoGeradoDeveTerSeisDigitos() {
        String codigoGerado = ValidatorUtils.gerarCodigoConfirmacao();
        assertEquals(6, codigoGerado.length());
    }

    @Test
    public void numeroDaContaDeveTerSeteCaracteresContandoOHifen() {
        String numeroConta = ValidatorUtils.gerarNumeroConta();
        assertEquals(7, numeroConta.length());
    }

    @Test
    public void numeroDaContaDevePossuirHifen() {
        String numeroConta = ValidatorUtils.gerarNumeroConta();
        boolean contemHifen = numeroConta.contains("-");
        assertTrue(contemHifen);
    }

    @Test
    public void numeroDaContaDevePossuirHifenNaPosicao5() {
        String numeroConta = ValidatorUtils.gerarNumeroConta();
        char hifen = numeroConta.charAt(5);
        assertEquals('-', hifen);
    }

    @Test
    public void deveRetornarTrueParaCpfQueTenhaOnzeDigitos() {
        boolean cpfValido = ValidatorUtils.verificarFormatoCpf("07515405015");
        assertTrue(cpfValido);
    }

    @Test
    public void deveRetornarTrueParaCnpjCom14Digitos() {
        boolean cnpjValido = ValidatorUtils.verificarFormatoCnpj("12345678900000");
        assertTrue(cnpjValido);
    }

    @Test
    public void deveRetornarTrueParaCpfQueTenhaOnzeDigitosPontosHifen() {
        boolean cpfValido = ValidatorUtils.verificarFormatoCpf("075.154.050-15");
        assertTrue(cpfValido);
    }

    @Test
    public void deveRetornarFalsoParaCpfComMenosDeOnzeDigitos() {
        boolean cpfInvalido = ValidatorUtils.verificarFormatoCpf("123123");
        assertFalse(cpfInvalido);
    }

    @Test
    public void deveRetornarFalsoParaCpfComMaisDeOnzeDigitos() {
        boolean cpfInvalido = ValidatorUtils.verificarFormatoCpf("123123123123123");
        assertFalse(cpfInvalido);
    }

    @Test
    public void deveRetornarTrueParaSenhaQueTenhaOitoDigitosUmaLetraMinusculaUmaMaisculaUmNumeroUmCaractereEspecial() {
        boolean senhaValida = ValidatorUtils.verificarSenhaDentroDoPadrao("Abc123@@");
        assertTrue(senhaValida);
    }

    @Test
    public void deveRetornarFalseParaSenhaQueNaoTenhaOitoDigitosMasTenhaOsCaracteresNecessarios() {
        boolean senhaInvalida = ValidatorUtils.verificarSenhaDentroDoPadrao("Abc123@");
        assertFalse(senhaInvalida);
    }

    @Test
    public void deveRetornarFalseParaSenhaQueTenhaOitoDigitosMasNaoTenhaNumero() {
        boolean senhaInvalida = ValidatorUtils.verificarSenhaDentroDoPadrao("Abc@@Cde");
        assertFalse(senhaInvalida);
    }

    @Test
    public void deveRetornarFalseParaSenhaQueTenhaOitoDigitosMasNaoTenhaCaractereEspecial() {
        boolean senhaInvalida = ValidatorUtils.verificarSenhaDentroDoPadrao("Abc12Cde");
        assertFalse(senhaInvalida);
    }

    @Test
    public void deveRetornarFalseParaSenhaQueTenhaOitoDigitosMasNaoTenhaLetraMinuscula() {
        boolean senhaInvalida = ValidatorUtils.verificarSenhaDentroDoPadrao("ABC@@123");
        assertFalse(senhaInvalida);
    }

    @Test
    public void deveRetornarFalseParaSenhaQueTenhaOitoDigitosMasNaoTenhaLetraMaiusculua() {
        boolean senhaInvalida = ValidatorUtils.verificarSenhaDentroDoPadrao("abc@@123");
        assertFalse(senhaInvalida);
    }

    @Test
    public void deveRetornarTrueParaCepInformadoComOitoNumerosSemHifen() {
        boolean cepValido = ValidatorUtils.validarCep("98765432");
        assertTrue(cepValido);
    }

    @Test
    public void deveRetornarTrueParaCepInformadoComCincoDigitosHifenTresDigitos() {
        boolean cepValido = ValidatorUtils.validarCep("98765-321");
        assertTrue(cepValido);
    }

    @Test
    public void deveRetornarFalseParaCepComMaisDeOitoDigitosSemHifen() {
        boolean cepInvalido = ValidatorUtils.validarCep("1239819283192");
        assertFalse(cepInvalido);
    }

    @Test
    public void deveRetornarFalseParaCepComHifenNaPosicaoErrada() {
        boolean cepInvalido = ValidatorUtils.validarCep("3123-3123");
        assertFalse(cepInvalido);
    }

    @Test
    public void deveRetornarFalseParaCepComMenosDeOitoDigitos() {
        boolean cepInvalido = ValidatorUtils.validarCep("1231");
        assertFalse(cepInvalido);
    }

    @Test
    public void deveRetornarTrueSeTelefoneTemDDQueComecaSemZeroMaisNoveNumeros() {
        boolean telefoneValido = ValidatorUtils.validarNumeroTelefone("11987654321");
        assertTrue(telefoneValido);
    }

    @Test
    public void deveRetornarTrueSeTelefoneTemDDQueComecaSemZeroMaisOitoNumeros() {
        boolean telefoneValido = ValidatorUtils.validarNumeroTelefone("1187654321");
        assertTrue(telefoneValido);
    }

    @Test
    public void deveRetornarFalseSeDDComecaEmZeroMasTemNoveDigitos() {
        boolean telefoneInvalido = ValidatorUtils.validarNumeroTelefone("00987654321");
        assertFalse(telefoneInvalido);
    }

    @Test
    public void deveRetornarFalseSeDDComecaEmZeroMasTemOitoDigitos() {
        boolean telefoneInvalido = ValidatorUtils.validarNumeroTelefone("0098765432");
        assertFalse(telefoneInvalido);
    }

    @Test
    public void deveRetornarFalseSeDDNaoComecaComZeroMasOPrimeiroDosNoveNumerosDiferenteDeNove() {
        boolean telefoneInvalido = ValidatorUtils.validarNumeroTelefone("11887654321");
        assertFalse(telefoneInvalido);
    }

    @Test
    public void deveRetornarTrueSeOsDoisCpfsInformadosForemValidosEForemIguais() {
        String cpf1 = "07515405015";
        String cpf2 = "07515405015";
        boolean cpfsIguais = ValidatorUtils.compararCpf(cpf1, cpf2);
        assertTrue(cpfsIguais);
    }

    @Test
    public void deveRetornarFalseSeUmDosCpfsInformadosForInvalido() {
        String cpf1 = "123457890";
        String cpf2 = "12345678900";
        boolean cpfsIguais = ValidatorUtils.compararCpf(cpf1, cpf2);
        assertFalse(cpfsIguais);
    }

    @Test
    public void deveRetornarFalseSeOsDoisCpfsForemIguaisMasInvalidos() {
        String cpf1 = "12345";
        String cpf2 = "12345";
        boolean cpfsIguais = ValidatorUtils.compararCpf(cpf1, cpf2);
        assertFalse(cpfsIguais);
    }

    @Test
    public void deveRetornarTrueSeAsDuasSenhasForemValidasEIguais() {
        String senha1 = "Abc123@@";
        String senha2 = "Abc123@@";
        boolean senhasIguais = ValidatorUtils.compararDuasSenhas(senha1, senha2);
        assertTrue(senhasIguais);
    }

    @Test
    public void deveRetornarFalseSeUmaDasSenhasForInvalida() {
        String senha1 = "abc123@@";
        String senha2 = "Abc123@@";
        boolean senhasIguais = ValidatorUtils.compararDuasSenhas(senha1, senha2);
        assertFalse(senhasIguais);
    }

    @Test
    public void deveRetornarFalseSeAsSenhasForemIguaisMasInvalidas() {
        String senha1 = "abc@@";
        String senha2 = "abc@@";
        boolean senhasIguais = ValidatorUtils.compararDuasSenhas(senha1, senha2);
        assertFalse(senhasIguais);
    }

    @Test
    public void deveAutenticarUsuarioQueInformaCpfSenhaValidosExistentes() {
        String cpf1 = "07515405015";
        String senha1 = "Abc123@@";
        String cpf2 = "07515405015";
        String senha2 = "Abc123@@";
        boolean usuarioAutenticado = ValidatorUtils.autorizarLogin(cpf1, senha1, cpf2, senha2);
        assertTrue(usuarioAutenticado);
    }

    @Test
    public void deveRetornarFalseSeUsuarioInformaCpfInvalidoOuNaoCadastrado() {
        String cpf1 = "1234567890";
        String senha1 = "Abc123@@";
        String cpf2 = "12345678900";
        String senha2 = "Abc123@@";
        boolean usuarioAutenticado = ValidatorUtils.autorizarLogin(cpf1, senha1, cpf2, senha2);
        assertFalse(usuarioAutenticado);
    }

    @Test
    public void deveRetornarFalseSeUsuarioInformaSenhaInvalidaOuNaoCadastrada() {
        String cpf1 = "12345678900";
        String senha1 = "Abc12";
        String cpf2 = "12345678900";
        String senha2 = "Abc123@@";
        boolean usuarioAutenticado = ValidatorUtils.autorizarLogin(cpf1, senha1, cpf2, senha2);
        assertFalse(usuarioAutenticado);
    }
    
    @Test
    public void deveRetornarTrueDataMenorQue25082003Recebendo25082002() throws ParseException {
    	String dataTest = "25/08/2002";
    	boolean test = ValidatorUtils.validaData(dataTest);
    	assertTrue(test);
    }
    
    @Test
    public void deveRetornarFalsoDataMaoirQue25082003Recebendo25092003() throws ParseException {
    	String dataTest = "25/09/2003";
    	boolean test = ValidatorUtils.validaData(dataTest);
    	assertFalse(test);
    }
    
    @Test
    public void deveRetornarTrueDataIgualA25082003Recebendo25082003() throws ParseException {
    	String dataTest = "25/08/2003";
    	boolean test = ValidatorUtils.validaData(dataTest);
    	assertTrue(test);
    }
}