package br.com.prowaybank.util;

import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidatorUtils {

	private static final SecureRandom random = new SecureRandom();

	/**
	 * Gera o codigo aleatorio para ser utilizado na redefinicao da senha
	 *
	 * @return uma string contendo um codigo aleatorio de 6 digitos
	 */
	public static String gerarCodigoConfirmacao() {
		return String.format("%06d", random.nextInt(999999));
	}
	
	/**
	 * Verifica codigo de confirma��o inserido
	 * 
	 * Faz a verificacao se o codigo aleatorio gerado eh igual ao codigo inserido
	 * 
	 * @param codigoInserido
	 * @return true se o codigo inserido for igual ao codigo gerado
	 */
	public static Boolean verificaCodigoIgual(String codigoInserido, String codigo) {
		return codigo.equals(codigoInserido);
	}

	/**
	 * Gera um número aleatório que será utilizado para representar o número da
	 * conta do cliente
	 *
	 * @return uma string contendo o número da conta do cliente no formato XXXXX-X
	 */
	public static String gerarNumeroConta() {
		String numeroConta = String.format("%06d", random.nextInt(999999));
		return numeroConta.substring(0, 5) + "-" + numeroConta.substring(5);
	}

	/**
	 * Verifica se o CPF informado encontra-se dentro do padrão.
	 *
	 * @param cpf String contendo o CPF a ser validado. Pode ser passado no formato
	 *            somente com os números (XXXXXXXXXXX) ou com pontos e hífen
	 *            (XXX.XXX.XXX-XX)
	 * @return true ou false conforme a verificação se o CPF informado encontra-se
	 *         ou não dentro do padrão esperado
	 */
	public static Boolean verificarFormatoCpf(String CPF) {
		CPF = formataCPF(CPF);

		if (CPF.equals("00000000000") || CPF.equals("11111111111") || CPF.equals("22222222222")
				|| CPF.equals("33333333333") || CPF.equals("44444444444") || CPF.equals("55555555555")
				|| CPF.equals("66666666666") || CPF.equals("77777777777") || CPF.equals("88888888888")
				|| CPF.equals("99999999999") || (CPF.length() != 11)) {

			return false;  
		} 

		char dig10, dig11;
		int sm, i, r, num, peso;

		// "try" - protege o codigo para eventuais erros de conversao de tipo (int)
		try {
			// Calculo do 1o. Digito Verificador
			sm = 0;
			peso = 10;
			for (i = 0; i < 9; i++) {
				num = (int) (CPF.charAt(i) - 48);
				sm = sm + (num * peso);
				peso = peso - 1;
			}

			r = 11 - (sm % 11);

			if ((r == 10) || (r == 11))
				dig10 = '0';
			else
				dig10 = (char) (r + 48); // converte no respectivo caractere numerico

			// Calculo do 2o. Digito Verificador
			sm = 0;
			peso = 11;
			for (i = 0; i < 10; i++) {
				num = (int) (CPF.charAt(i) - 48);
				sm = sm + (num * peso);
				peso = peso - 1;
			}

			r = 11 - (sm % 11);

			if ((r == 10) || (r == 11))
				dig11 = '0';
			else
				dig11 = (char) (r + 48);

			// Verifica se os digitos calculados conferem com os digitos informados.
			if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)))
				return true;
			else
				return false;

		} catch (InputMismatchException erro) {
			return false;
		}
	}

	/**
	 * Vai receber um CPF e remover o "." e o "-" para validacao
	 * 
	 * @param cpf
	 * @return
	 */
	private static String formataCPF(String cpf) {
		if (cpf != null) {
			cpf = cpf.replace(".", "");
			cpf = cpf.replace("-", "");
			return cpf;
		}
		return "00000000000";
	}

	/**
	 * Verifica se CNPJ digitado e valido
	 * 
	 * @param CNPJ
	 * @return true para cnpj valido e false para cnpj invalido
	 */
	public static Boolean verificarFormatoCnpj(String CNPJ) {
		Pattern p = Pattern.compile(RegexPatterns.CPF_CNPJ_PATTERN.getPadrao());
		Matcher m = p.matcher(CNPJ);

		return m.matches();
	}

	/**
	 * Verifica se a senha que foi informado encontra-se dentro do padrão.
	 * Espera-se que a senha tenha no mínimo 8 dígitos, pelo menos um número,
	 * pelo menos um caractere especial (!@#$%&¨), pelo menos uma letra minúscula
	 * e pelo menos uma letra maiúscula. Também verifica se a senha não possui
	 * nenhum espaço em branco em qualquer posição da string.
	 * 
	 * @param senha Recebe uma string contendo a senha
	 * @return o resultado da verificação. true se a senha está dentro do padrão
	 *         e false se está fora do padrão.
	 */
	public static Boolean verificarSenhaDentroDoPadrao(String senha) {
		Pattern p = Pattern.compile(RegexPatterns.PASSWORD_PATTERN.getPadrao());
		Matcher m = p.matcher(senha);

		return m.matches();
	}

	/**
	 * Valida o CEP informado
	 *
	 * @param cep String contendo o CEP, podendo ser no formato XXXXXXXX ou
	 *            XXXXX-XXX
	 * @return o resultado da validação, sendo true para o caso de estar no
	 *         formato esperado ou false se estiver fora.
	 */
	public static Boolean validarCep(String cep) {
		Pattern p = Pattern.compile(RegexPatterns.CEP_PATTERN.getPadrao());
		Matcher m = p.matcher(cep);

		return m.matches();
	}

	/**
	 * Verifica se o telefone informado está de acordo com o padrão esperado
	 * XXXXXXXXXXX. Faz a validação se o DD possui dois dígitos entre 1 e 9 cada.
	 * Verifica se o primeiro dígito é 9, que pode ou não existir. Os demais
	 * dígitos estão entre 0 e 9.
	 *
	 * @param numeroTelefone recebe o número de telefone tanto no formato
	 * @return o resultado da validação. true se o número informado está no
	 *         padrão e false se está fora do padrão
	 */
	public static Boolean validarNumeroTelefone(String numeroTelefone) {
		if (numeroTelefone != null) {
			Pattern p = Pattern.compile(RegexPatterns.PHONE_PATTERN.getPadrao());
			Matcher m = p.matcher(numeroTelefone);	
			return m.matches();
		}
		return false;
	}

	/**
	 * Compara dois CPFs informados para ver se são iguais e permitir a
	 * autenticação do usuário.
	 * 
	 * @param cpf1 String - CPF que é informado na hora do login
	 * @param cpf2 String - CPF que está cadastrado no sistema
	 * @return true se os CPFs informados são iguais ou false se são diferentes
	 */
	public static Boolean compararCpf(String cpf1, String cpf2) {
		if (!verificarFormatoCpf(cpf1) || !verificarFormatoCpf(cpf2)) {
			return false;
		}

		return cpf1.equals(cpf2);
	}

	/**
	 * Compara duas senhas para verificar se são iguais. Primeiro faz a
	 * verificação para ver se ambas as senhas são válidas e depois compara se
	 * são iguais. Método que pode ser utilizado na hora de fazer o login ou na
	 * hora de redefinir a senha.
	 * 
	 * @param senha1 String - primeira senha
	 * @param senha2 String - segunda senha
	 * @return o resultado da comparação. Retorna true se as senhas forem válidas
	 *         e iguais, e false caso sejam inválidas ou diferentes.
	 */
	public static Boolean compararDuasSenhas(String senha1, String senha2) {
		if (!verificarSenhaDentroDoPadrao(senha1) || !verificarSenhaDentroDoPadrao(senha2)) {
			return false;
		}

		return senha1.equals(senha2);
	}

	/**
	 * Faz a autenticação do usuário. Recebe a senha e o cpf do usuário e faz a
	 * verificação se elas são válidas e se já estão cadastradas.
	 * 
	 * @param cpf1
	 * @param senha1
	 * @param cpf2
	 * @param senha2
	 * @return
	 */
	public static boolean autorizarLogin(String cpf1, String senha1, String cpf2, String senha2) {

		return compararCpf(cpf1, cpf2) && compararDuasSenhas(senha1, senha2);
	}

	/**
	 * Verifica que se data fornecida e anterior a data atual
	 * 
	 * O metodo tambem verifica se a data fornecida pelo usuario e anterior a
	 * 25/08/2003 garantindo que o usuario tenha mais de 18 anos
	 * 
	 * @param data
	 * @return true para data valida e false para data invalida
	 * @throws ParseException
	 */
	public static boolean validaData(String data1) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dataAtual = new Date();
		Date dataCorte = sdf.parse("25/08/2003");//
		Date data = sdf.parse(data1);
		if (data.before(dataAtual)) {
			if (data.compareTo(dataCorte) <= 0)
				return true;
		}
		return false;
	}  
	 /**
	  * Verifica se  data inicial da empresa � menor ou igual com a data atual.
	  * @param data1
	  * @return   true para data valida 
	  * @throws ParseException
	  */
	public static boolean dataCriacao(String data1) throws ParseException { 
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dataAtual = new Date();
		Date data = sdf.parse(data1);
		if (data.before(dataAtual)) {
		
			return true;
		
		}  
		return false;
	}
}


