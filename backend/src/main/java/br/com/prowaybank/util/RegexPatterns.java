package br.com.prowaybank.util;

public enum RegexPatterns {

    CPF_CNPJ_PATTERN("(^\\d{3}\\.?\\d{3}\\.?\\d{3}\\-?\\d{2}$)|(^\\d{2}\\.?\\d{3}\\.?\\d{3}\\/?\\d{4}\\-?\\d{2}$)"),
    PASSWORD_PATTERN("(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%&])(?=\\S+$).{8,}"),
    CEP_PATTERN("[0-9]{5}-?[0-9]{3}"),
    PHONE_PATTERN("[1-9]{2}[9]?[0-9]{4}[0-9]{4}");

    private String padrao;

    RegexPatterns(String padrao) {
        this.padrao = padrao;
    }

    public String getPadrao() {
        return padrao;
    }
}
