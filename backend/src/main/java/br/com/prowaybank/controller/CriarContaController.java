package br.com.prowaybank.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.prowaybank.conceitospoo.ContaBancaria;
import br.com.prowaybank.interfaces.CadastroUsuario;
import br.com.prowaybank.util.ValidatorUtils;

public class CriarContaController {
	static String listContas[] = {"12345-4", "65478-8", "35481-7", "67841-9"};
	static String listCPF[] = {"523.960.620-00", "98031597049", "394.327.210-94", "57056858000195"};
	static String agencia = "1152-5";
	/**
	 * Metodo para realizar o cadastro de um novo usuario no banco
	 * 
	 * O metodo recebe o objeto PessoaFisica gerada no front end e valida as
	 * informacoes recebidas para garantir que todos os dados estao corretos antes
	 * de enviar para o banco de dados e retornar uma mensagem confirmando o
	 * cadastro
	 *  
	 * @param pessoaFisica
	 * @throws Exception 
	 */
	public static boolean criarConta(CadastroUsuario usuario, String senha1, String senha2) throws Exception {
		if (!usuario.validarDocumento(usuario.getDocumento())) {
			throw new Exception("Documento invalido!");
		}
		
		boolean dadosValidos = CriarContaController.dadosValidos(usuario.getTelefone(),
				usuario.getDataInicial());//verifica se os dados sao validos
		
		boolean senhasIguais = ValidatorUtils.compararDuasSenhas(senha1, senha2);
		
		if (dadosValidos && senhasIguais) {
			if (CriarContaController.consultaDocumento(usuario.getDocumento())) {//Verifica se o CPF pode ser usado	
				boolean numeroPermitido = false;
				String numeroConta;
				do {
					numeroConta = ValidatorUtils.gerarNumeroConta();
					numeroPermitido = verificarNumeroConta(numeroConta);
				} while (numeroPermitido);
		
				ContaBancaria novaConta = new ContaBancaria(agencia, numeroConta, null);//Cria a nova conta bancaria
				usuario.setContaBancaria(novaConta);
				usuario.setSenha(senha1);
				usuario.setDataAberturaConta(new Date());
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo para verificar se o numero gerado ja existe no banco
	 * 
	 * Esse metodo faz uma consulta ao banco de dados, retorna a lista de contas e
	 * verifica se o numeroGerado ja esta vinculado a alguma conta existente
	 * @param numeroGerado
	 * @return false caso ja exista numero true caso seja possivel usar o numero
	 */
	private static boolean verificarNumeroConta(String numeroGerado) {
		for(int i = 0; i < listContas.length; i++) { //temporario ate ser possivel pegar do banco
			if(listContas[i] == numeroGerado) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Verificar se o CPF recebido ja existe no banco
	 * @param cpf
	 * @return false para CPF existente e true para CPF liberado
	 */
	public static boolean consultaDocumento(String documento) {	
		for(int i = 0; i < listCPF.length; i++) { //temporario ate ser possivel pegar do banco
			if(listCPF[i] == documento) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Valida os principais dados do usuario no momento do cadastro
	 * 
	 * @param cpf
	 * @param telefone
	 * @param data
	 * @return true para dados validos e false em caso de erro
	 * @throws ParseException
	 */
	public static boolean dadosValidos(String telefone, Date data) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dataFormatada = sdf.format(data);

		boolean telefoneValido = ValidatorUtils.validarNumeroTelefone(telefone);
		boolean dataNacimentoValida = ValidatorUtils.validaData(dataFormatada);

		if (telefoneValido && dataNacimentoValida)
			return true;
		else
			return false;

	}
}
