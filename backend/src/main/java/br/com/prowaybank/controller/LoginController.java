package br.com.prowaybank.controller;

import br.com.prowaybank.conceitospoo.PessoaFisica;
import br.com.prowaybank.conceitospoo.PessoaJuridica;
import br.com.prowaybank.conceitospoo.Usuario;
import br.com.prowaybank.util.ValidatorUtils;

public class LoginController {

	/**
	 * Realiza o login do usuario no sistema
	 * @param usuario
	 * @param login
	 * @param senha
	 * @return true para login bem sucessido e false quando houver erro
	 */
    public boolean realizarLogin(Usuario usuario, String login, String senha) {

        if (usuario instanceof PessoaFisica) {
            String cpfCadastrado = ((PessoaFisica) usuario).getCpf();
            String senhaCadastrada = usuario.getSenha();
            return ValidatorUtils.autorizarLogin(cpfCadastrado, senhaCadastrada, login, senha);
        }

        if (usuario instanceof PessoaJuridica) {
            String cnpjCadastrado = ((PessoaJuridica) usuario).getCnpj();
            String senhaCadastrada = usuario.getSenha();
            return ValidatorUtils.autorizarLogin(cnpjCadastrado, senhaCadastrada, login, senha);
        }

        return false;
    }

    /**
     * Verifica qual o tipo de usuario esta requisitando login
     * @param usuario
     * @param login
     * @param senha
     * @return loginPf ou loginPj
     */
    public boolean verificarTipoLogin(Usuario usuario, String login, String senha) {
        if (usuario instanceof PessoaFisica) {
            return loginPf((PessoaFisica) usuario, login, senha);
        } else if (usuario instanceof PessoaJuridica) {
            return loginPj((PessoaJuridica) usuario, login, senha);
        }

        return false;
    }

    /**
     * Verifica o login para Pf
     * @param pf
     * @param cpf
     * @param senha
     * @return true para login bem sucessido e false quando houver erro
     */
    public boolean loginPf(PessoaFisica pf, String cpf, String senha) {
        String cpfCadastrado = pf.getCpf();
        String senhaCadastrada = pf.getSenha();
        return ValidatorUtils.autorizarLogin(cpfCadastrado, senhaCadastrada, cpf, senha);
    }

    /**
     * Verifica o login para Pj
     * @param pj
     * @param cnpj
     * @param senha
     * @return true para login bem sucessido e false quando houver erro
     */
    public boolean loginPj(PessoaJuridica pj, String cnpj, String senha) {
        String cnpjCadastrado = pj.getCnpj();
        String senhaCadastrada = pj.getSenha();
        return ValidatorUtils.autorizarLogin(cnpjCadastrado, senhaCadastrada, cnpj, senha);
    }
}
