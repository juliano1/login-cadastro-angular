package br.com.prowaybank.controller;

import br.com.prowaybank.conceitospoo.PessoaFisica;
import br.com.prowaybank.conceitospoo.PessoaJuridica;

public class CadastrarUsuarioController {

	public void criarPf(PessoaFisica novaPf, String senha1, String senha2) throws Exception {		
		PessoaFisica pessoaF = new PessoaFisica(novaPf.getNome(), novaPf.getTelefone(), novaPf.getEmail(),
				novaPf.getEndereco(), novaPf.getDocumento(), novaPf.getRendimento(), novaPf.getDataInicial());
		
		CriarContaController.criarConta(pessoaF, senha1, senha2);
	}

	
	
	public void criarPj(PessoaJuridica novoPj, String senha1, String senha2) throws Exception {
		PessoaJuridica pessoaJ = new PessoaJuridica(novoPj.getNome(), novoPj.getTelefone(), novoPj.getEmail(),
				novoPj.getEndereco(), novoPj.getDocumento(), novoPj.getRazaoSocial(), novoPj.getCapitalInicial(),
				novoPj.getAporteMensal(), novoPj.getEnderecoEmpresa(), novoPj.getDataInicial(), novoPj.getFaturamentoAnual(),
				novoPj.getCpfRepresentante());
		
		CriarContaController.criarConta(pessoaJ, senha1, senha2);
	}
}
