package br.com.prowaybank.controller;

import br.com.prowaybank.interfaces.AlteraSenha;
import br.com.prowaybank.util.ValidatorUtils;

public class RedefinirSenhaController  {
	
	String codigo = ValidatorUtils.gerarCodigoConfirmacao();

	/**
	 * Altera senha para conta do usuario
	 * 
	 * Verifica se o cpf/cnpj ja existe na base de dados 
	 * 
	 * @param usuario: interface que implementa validacoes
	 * @param Documento: Cpf/Cnpj do usuario
	 * @param novaSenha: Nova senha desejada
	 * @param codigoConfirmacao: C�digo de confirmacao gerado
	 * 
	 * @return novaSenha: Senha nova alterada p/ usuario
	 */
	public void alterarSenha (AlteraSenha usuario, String documento, String novaSenha, String confirmaSenha, String codigoConfirmacao) {
		Boolean senhasIguais = novaSenha.equals(confirmaSenha);
		Boolean codigoValido = usuario.validaCodigoConfirmacao(codigo, codigoConfirmacao); 
		Boolean documentoCadastrado = usuario.verificaDocumentoCadastrado(documento);
		Boolean documentoValido = usuario.validarDocumento(documento);
		
		if(codigoValido && documentoCadastrado && documentoValido && senhasIguais) {
			usuario.setSenha(novaSenha);
		}
	}
}