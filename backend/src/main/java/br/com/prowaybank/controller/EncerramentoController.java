package br.com.prowaybank.controller;

import java.util.ArrayList;

import br.com.prowaybank.interfaces.EncerraConta;

public class EncerramentoController {
	/**
	 * Encerra conta do usu�rio
	 * 
	 * @param usuario, documento (CPF ou CNPJ)
	 * @return mensagem de sucesso
	 */
	public String encerrarContaCliente (EncerraConta usuario, String documento) {
		ArrayList<String> pendencias = usuario.getPendencias(documento);
		
		if(pendencias.isEmpty()) {
			usuario.encerrarConta(documento);
			return "Conta encerrada com sucesso";
		} else return pendencias.get(0);
	}

}
