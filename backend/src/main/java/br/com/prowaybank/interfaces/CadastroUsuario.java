package br.com.prowaybank.interfaces;

import java.math.BigDecimal;
import java.util.Date;

import br.com.prowaybank.conceitospoo.ContaBancaria;
import br.com.prowaybank.conceitospoo.Endereco;

public interface CadastroUsuario extends ValidaDocumentos {

	public String getTelefone();
	public Date getDataInicial();
	public Endereco getEndereco();
	public String getDocumento();
	public String getNome();
	public String getEmail();
	public BigDecimal getRendimento();
	
	public void setContaBancaria(ContaBancaria contaBancaria);
	public void setSenha(String senha);
	public void setDataAberturaConta(Date dataAberturaConta);
}
