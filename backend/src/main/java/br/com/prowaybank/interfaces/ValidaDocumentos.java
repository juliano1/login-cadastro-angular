package br.com.prowaybank.interfaces;

//import br.com.prowaybank.conceitospoo.Usuario;

public interface ValidaDocumentos {
	
	public Boolean validarDocumento (String documento);
	
	public Boolean verificaDocumentoCadastrado (String documento);
	
	public Boolean validaCodigoConfirmacao (String codigoInserido, String codigo);
}
