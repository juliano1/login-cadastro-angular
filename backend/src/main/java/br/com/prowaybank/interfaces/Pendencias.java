package br.com.prowaybank.interfaces;

import java.util.ArrayList;

public interface Pendencias {
	public ArrayList<String> getPendencias(String documento);
}
