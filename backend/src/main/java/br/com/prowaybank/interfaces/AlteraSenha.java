package br.com.prowaybank.interfaces;

public interface AlteraSenha extends ValidaDocumentos {
	
	public void setSenha(String senha);
	public String getSenha();
}
