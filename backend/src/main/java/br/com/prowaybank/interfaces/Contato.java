package br.com.prowaybank.interfaces;

import br.com.prowaybank.conceitospoo.Endereco;

public interface Contato  {
	public String getTelefone();
	public String getEmail();
	public Endereco getEndereco();

}
