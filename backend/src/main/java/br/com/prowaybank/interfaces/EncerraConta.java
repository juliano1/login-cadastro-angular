package br.com.prowaybank.interfaces;

public interface EncerraConta extends Pendencias {
	public Boolean encerrarConta(String documento);
}
