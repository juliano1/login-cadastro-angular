package br.com.prowaybank.conceitospoo;

import java.math.BigDecimal;

public class ContaBancaria {

    private Long id;

    private String agencia;

    private String numeroConta;

    private BigDecimal valorEmConta;
    
    private Boolean ativa;

    public ContaBancaria() {
    	this.setAtiva(true);
    }
    
    public ContaBancaria(String agencia, String numeroConta, BigDecimal valorEmConta) {
		this.agencia = agencia;
		this.numeroConta = numeroConta;
		this.valorEmConta = valorEmConta;
		this.setAtiva(true);
	}

    
	/**
     * Retorna o ID do objeto ContaBancaria
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * Retorna a agência da ContaBancaria
     *
     * @return String agencia
     */
    
    
    
    public String getAgencia() {
        return agencia;
    }

    /**
     * Modifica a agência vinculada à conta bancária
     *
     * @param agencia
     */
    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    /**
     * Retorna o número da conta bancária que está viculada ao cliente
     *
     * @return String número da conta
     */
    public String getNumeroConta() {
        return numeroConta;
    }

    /**
     * Modifica o número da conta bancária vinculada ao cliente
     *
     * @param numeroConta
     */
    public void setNumeroConta(String numeroConta) {
        this.numeroConta = numeroConta;
    }

    /**
     * Exibe o valor total disponível na conta bancária
     *
     * @return BigDecimal com o valor disponível
     */
    public BigDecimal getValorEmConta() {
        return valorEmConta;
    }

    /**
     * Modifica o valor disponível em conta
     *
     * @param valorEmConta
     */
    public void setValorEmConta(BigDecimal valorEmConta) {
        this.valorEmConta = valorEmConta;
    }

	public Boolean getAtiva() {
		return ativa;
	}

	public void setAtiva(Boolean ativa) {
		this.ativa = ativa;
	}
}
