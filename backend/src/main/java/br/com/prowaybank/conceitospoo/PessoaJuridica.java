package br.com.prowaybank.conceitospoo;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import br.com.prowaybank.interfaces.AlteraSenha;
import br.com.prowaybank.interfaces.CadastroUsuario;
import br.com.prowaybank.interfaces.EncerraConta;
import br.com.prowaybank.util.ValidatorUtils;



public class PessoaJuridica extends Usuario implements AlteraSenha, EncerraConta, CadastroUsuario {

    private String cnpj;

    private String razaoSocial;

    private BigDecimal capitalInicial;

    private BigDecimal aporteMensal;
    
    private Endereco enderecoEmpresa;
    
    private Date dataEmpresa;  
    
    private BigDecimal faturamentoAnual;
    
    private String cpfRepresentante; // fazendo aqui
 
    private Date dataAberturaConta;
   


    public PessoaJuridica() {
    }    
    
    public PessoaJuridica(String nome, String telefone, String email, Endereco endereco, String cnpj,
			String razaoSocial, BigDecimal capitalInicial, BigDecimal aporteMensal, Endereco enderecoEmpresa,
			Date dataEmpresa, BigDecimal faturamentoAnual, String cpfRepresentante) {
		super(nome, telefone, email, endereco);
		this.cnpj = cnpj;
		this.razaoSocial = razaoSocial;
		this.capitalInicial = capitalInicial;
		this.aporteMensal = aporteMensal;
		this.enderecoEmpresa = enderecoEmpresa;
		this.dataEmpresa = dataEmpresa;
		this.faturamentoAnual = faturamentoAnual;
		this.cpfRepresentante = cpfRepresentante;
	}


	/**
     * Exibe o CNPJ da pessoa jurídica
     *
     * @return String com o CNPJ
     */
    public String getCnpj() {
        return cnpj;
    }

    /**
     * Modifica o CNPJ da pessoa jurídica
     *
     * @param cnpj
     */
    public void setCnpj(String cnpj) {
    	if(ValidatorUtils.verificarFormatoCnpj (cnpj))
        this.cnpj = cnpj;
    }

    /**
     * Exibe a razão social da empresa/pessoa jurídica
     *
     * @return String com  razão social
     */
    public String getRazaoSocial() {
        return razaoSocial;
    }

    /**
     * Modifica a razao social da empresa/pessoa jurídica
     *
     * @param razaoSocial
     */
    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    /**
     * Retorna o valor do capital inicial da PJ
     *
     * @return BigDecimal com o capital inicial
     */
    public BigDecimal getCapitalInicial() {
        return capitalInicial;
    }

    /**
     * Modifica o capital inicial da PJ
     *
     * @param capitalInicial
     */
    public void setCapitalInicial(BigDecimal capitalInicial) {
        this.capitalInicial = capitalInicial;
    }

    /**
     * Retorna o valor dos aportes mensais feitos pela PJ
     *
     * @return BigDecimal com o valor do aporte mensal
     */
    public BigDecimal getAporteMensal() {
        return aporteMensal;
    }

    /**
     * Modifica o valor do aporte mensal da PJ
     *
     * @param aporteMensal
     */
    public void setAporteMensal(BigDecimal aporteMensal) {
        this.aporteMensal = aporteMensal;
    }
    /**
     * Retorna o endere�o da empresa
     * 
     * @return Endereco com endere�o empresa
     */
	public Endereco getEnderecoEmpresa() {
		return enderecoEmpresa;
	}

	/**
	 * modifica  endere�o  de enderecoEmpresa
	 * 
	 * @param enderecoEmpresa
	 */
	public void setEnderecoEmpresa(Endereco enderecoEmpresa) {
		this.enderecoEmpresa = enderecoEmpresa; 
	}
	/**
	 * Retorna a data de cria��o da empresa
	 * 
	 * @return Date  com dataEmpresa 
	 */
	public Date getDataEmpresa() {
		return dataEmpresa;
	}
	/**
	 * Informa a data de criacao da empresa.
	 * 
	 * @param dataEmpresa
	 * @throws ParseException 
	 */
	public void setDataEmpresa(Date dataEmpresa) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dataFormatada = sdf.format(dataEmpresa);  // Formata data ( date/String ).
        if (ValidatorUtils.dataCriacao(dataFormatada)) {
		this.dataEmpresa = dataEmpresa;
		}
	}
	/**
	 * Informa o numero de telefone da pessoa juridica
	 * 
	 * @return string com telefone
	 */

	/**
	 * Informa o rendimento anual da empresa
	 * 
	 * @return  BigDecimal com faturamento anual
	 */
	public BigDecimal getFaturamentoAnual() {
		return faturamentoAnual;
	}
	/**
	 * modifica o rendimento anual da empresa.
	 * 
	 * @param faturamentoAnual
	 */
	public void setFaturamentoAnual(BigDecimal faturamentoAnual) {
		this.faturamentoAnual = faturamentoAnual;
	}
	/**
	 * Informa o cpf do representnte
	 * 
	 * @return String cpf do representante
	 * 
	 */
	public String getCpfRepresentante() {
		return cpfRepresentante;
	}
	/**
	 * Modifica o cpf do representante
	 * 
	 * @param cpfRepresentante
	 */
	public void setCpfRepresentante(String cpfRepresentante) {
		if(ValidatorUtils. verificarFormatoCpf(cpfRepresentante))
		this.cpfRepresentante = cpfRepresentante;
	}
	/**
	 * Informa o RG do representante
	 * 
	 * @return String RG do representante
	 */

   /**
     * Retorna o endere�o do reprsentante da emprasa
     * 
     * @return Endereco com endere�o Representante
     */

	@Override
	public Boolean validarDocumento(String CNPJ) {
		return ValidatorUtils.verificarFormatoCnpj(CNPJ);
	}

	@Override
	public Boolean verificaDocumentoCadastrado(String CNPJ) {
		String cnpjCadastrado = this.getCnpj();
		return cnpjCadastrado.equals(CNPJ);
	}

	@Override
	public Boolean validaCodigoConfirmacao(String codigoInserido, String codigoConfirmacao) {
		return codigoConfirmacao.equals(codigoInserido);
	}

	@Override

	public Date getDataInicial() {
		return this.dataEmpresa;
	}

	@Override
	public String getDocumento() {
		return this.cnpj;
	}

	@Override
	public BigDecimal getRendimento() {
		return this.aporteMensal;
	}

	public Date getDataAberturaConta() {
		return dataAberturaConta;
	}

	public void setDataAberturaConta(Date dataAberturaConta) {
		this.dataAberturaConta = dataAberturaConta;
	}
	
	public ArrayList<String> getPendencias(String documento) {
		ArrayList<String> pendencias = new ArrayList<String>();
		
		PessoaJuridica usuario = new PessoaJuridica();
		String cnpjCadastrado = usuario.getCnpj();
		
		if (cnpjCadastrado.equals(documento)){
			if(((Usuario) usuario).getContaBancaria().getAtiva()) {
				if(((Usuario) usuario).getContaBancaria().getValorEmConta().compareTo(BigDecimal.ZERO) > 0) {
					pendencias.add("Empresa possui saldo em conta corrente. Imposs�vel encerrar a conta.");
				}
			} else {
				pendencias.add("Empresa n�o possui conta corrente ativa. Imposs�vel encerrar a conta.");
			}
		}
		return pendencias;
	}

	@Override
	public Boolean encerrarConta(String documento) {
		String cnpjCadastrado = this.getCnpj();
		
		if (cnpjCadastrado.equals(documento)){
			((Usuario) this).getContaBancaria().setAtiva(false);
		}
		
		return ((Usuario) this).getContaBancaria().getAtiva();

	}
}
