package br.com.prowaybank.conceitospoo;

import br.com.prowaybank.util.ValidatorUtils;

public class Endereco {

    private Long id;

    private String logradouro;

    private String numero;

    private String cep;

    private String cidade;

    private String estado;

    /**
     * Id do endereço
     *
     * @return o Id do endereço
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Retorna o logradouro (nome da rua, avenida, etc) do endereço
     *
     * @return logradouro
     */
    public String getLogradouro() {
        return this.logradouro;
    }

    /**
     * Modifica o logradouro
     *
     * @param logradouro
     */
    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    /**
     * Retorna o número da rua/avenida/etc do endereço do usuário
     *
     * @return número da rua no formato de string
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Modifica o número da rua/avenida/etc
     *
     * @param numero
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * Retorna o CEP do endereço do usuário
     *
     * @return String CEP
     */
    public String getCep() {
        return cep;
    }

    /**
     * Modifica o CEP do usuário. Só faz a modificação se o CEP informado estiver dentro do padrão definido
     * no método validarCep(), que deve receber um CEP ou no formato XXXXXXXX ou XXXXX-XXX
     * @param cep
     */
    public void setCep(String cep) {
        if(ValidatorUtils.validarCep(cep)) {
            this.cep = cep;
        }
    }

    /**
     * Retorna a cidade do usuário
     *
     * @return String cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * Modifica a cidade do usuário
     *
     * @param cidade
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * Retorna o estado (UF) do usuário
     *
     * @return String estado (UF)
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Modifica o estado (UF) do usuário
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
}
