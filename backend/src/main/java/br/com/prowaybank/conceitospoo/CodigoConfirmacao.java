package br.com.prowaybank.conceitospoo;

public class CodigoConfirmacao extends Usuario {
	
	String codigo;
	String validade;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getValidade() {
		return validade;
	}
	public void setValidade(String validade) {
		this.validade = validade;
	}
}
