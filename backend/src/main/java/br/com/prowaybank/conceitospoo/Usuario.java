package br.com.prowaybank.conceitospoo;

import br.com.prowaybank.util.ValidatorUtils;

public class Usuario {

	private Long id; // permanece

	private String nome; // inteface

	private String telefone; // interface contato

	private String senha;// permanece

	private String email;// intefce contato

	private Endereco endereco; // interface contato

	private ContaBancaria contaBancaria;

	public Usuario() {
	}

	public Usuario(String nome, String telefone, String email, Endereco endereco) {
		this.nome = nome;
		this.telefone = telefone;
		this.email = email;
		this.endereco = endereco;
	}

	/**
	 * ID do usuário
	 *
	 * @return Long com o id do usuário
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Nome do usuário
	 *
	 * @return string com o nome do usuário
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Modifica o nome do usuário
	 *
	 * @param nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Exibe o telefone do usuário
	 *
	 * @return string contendo o telefone do usuário
	 */
	public String getTelefone() {
		return telefone;
	}

	/**
	 * Modifica o telefone do usuário
	 *
	 * @param telefone
	 */
	public void setTelefone(String telefone) {
		if (ValidatorUtils.validarNumeroTelefone(telefone)) { // aqui
			this.telefone = telefone;
		}
	}

	/**
	 * Exibe o endereço do usuário
	 *
	 * @return um objeto do tipo Endereco, onde estão cadastradas todas as
	 *         informações do endereço
	 */
	public Endereco getEndereco() {
		return endereco;
	}

	/**
	 * Modifica o endereço do usuário.
	 *
	 * @param endereco Recebe um objeto do tipo Endereco, onde estão todas as
	 *                 informações do endereço do usuário
	 */
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	/**
	 * Exibe a conta bancária do cliente. Retorna um objeto do tipo ContaBancaria
	 * que contém as informações da conta, como agência e número da conta
	 *
	 * @return objeto ContaBancaria com as informações da conta do usuário
	 */
	public ContaBancaria getContaBancaria() {
		return contaBancaria;
	}

	/**
	 * Modifica a conta bancária do usuário
	 *
	 * @param contaBancaria - objeto do tipo ContaBancaria, que contém a agência e
	 *                      o número da conta
	 */
	public void setContaBancaria(ContaBancaria contaBancaria) {
		this.contaBancaria = contaBancaria;
	}

	/**
	 * Retorna a senha do usuário
	 *
	 * @return String com a senha
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * Modifica a senha do usuário
	 *
	 * @param senha
	 */
	public void setSenha(String senha) {
		if (ValidatorUtils.verificarSenhaDentroDoPadrao(senha)) {
			this.senha = senha;
		}
	}

	/**
	 * Exibe o e-mail do usuário
	 *
	 * @return String contendo o e-mail
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Cadastra o e-mail do usuário
	 *
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

}
